const cors = require('cors');
const express = require('express');
const app = express();
const path = require('path');

// ��������� ������� �� ���� ����������
app.use(cors());

// ��������� Express, ��� ��������� ����������� �����
app.use(express.static(__dirname));

// �������������: ��������� �������� ��� �����, ���� ����� ������ index.html ����
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '', 'index.html'));
});

app.listen(8089, () => {
    console.log('Server is running on http://localhost:8089');
});